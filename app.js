//finished version of the app.js file
"use strict";
const express = require("express");
const app = express();
const port = process.env.port || 8080;
const things=require("./routes/things");

app.use(express.json());
app.use("/things",require("./routes/things.js"));

//use the thing.js to handel the endpoints



app.get("/", (req, res) => {
  //handle root
  res.send("hello root");
});



app.listen(port,err =>{
  if(err){
    return console.log("error",err);
  }
  console.log(`listening to port${port}`);
})

