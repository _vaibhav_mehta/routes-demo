"use strict";
const express=require("express");
let router=express.Router();

router.use(function(req,res,next){
    console.log(req.url);
    next();
})

router
.route("/cars")
.get((req,res)=>{
    res.json("hii this is get /things/cars");
})
.post((req,res)=>{
    res.json("hii this is post /things/cars");
})


router
.route("/cars/:carid")
.get((req,res)=>{
    res.send("hii this is get /things/cars"+req.params.carid);
})
.put((req,res)=>{
    res.send("hii this is put /things/cars"+req.params.carid);
})

module.exports = router;

